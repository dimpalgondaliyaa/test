<?php namespace Category\Catdemo;

use Backend;
use System\Classes\PluginBase;

/**
 * catdemo Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'catdemo',
            'description' => 'No description provided yet...',
            'author'      => 'category',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        
        

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Category\Catdemo\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'category.catdemo.some_permission' => [
                'tab' => 'catdemo',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        /*return [];*/ // Remove this line to activate

        return [
            'catdemo' => [
                'label'       => 'catdemo',
                'url'         => Backend::url('category/catdemo/addcats'),
                'icon'        => 'icon-leaf',
                'permissions' => ['category.catdemo.*'],
                'order'       => 500,
            ],
        ];
    }
}
