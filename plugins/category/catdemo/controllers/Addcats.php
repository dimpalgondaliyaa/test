<?php namespace Category\Catdemo\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Addcats Back-end Controller
 */
class Addcats extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Category.Catdemo', 'catdemo', 'addcats');
    }
}
