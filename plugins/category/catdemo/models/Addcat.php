<?php namespace Category\Catdemo\Models;

use Model;
use Rafie\Sitepointdemo\Models\Team;

/**
 * Addcat Model
 */
class Addcat extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'category_catdemo_addcats';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = ['posts' => ['Rafie\Sitepointdemo\Models\Team', 'table' => 'rafie_sitepointdemo_three_cat']];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
