<?php namespace Category\Catdemo\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAddcatsTable extends Migration
{
    public function up()
    {
        Schema::create('category_catdemo_addcats', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('catname');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('category_catdemo_addcats');
    }
}
