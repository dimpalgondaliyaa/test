<?php namespace Rafie\SitepointDemo;

use Backend;
use System\Classes\PluginBase;

/**
 * sitepointDemo Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'sitepointDemo',
            'description' => 'No description provided yet...',
            'author'      => 'rafie',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Rafie\SitepointDemo\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'rafie.sitepointdemo.some_permission' => [
                'tab' => 'sitepointDemo',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        /*return [];*/ // Remove this line to activate

        return [
            'sitepointdemo' => [
                'label'       => 'sitepointDemo',
                'url'         => Backend::url('rafie/sitepointdemo/teams'),
                'icon'        => 'icon-leaf',
                'permissions' => ['rafie.sitepointdemo.*'],
                'order'       => 500,
            ],
        ];
    }
}
