<?php namespace Rafie\Sitepointdemo\Models;

use Model;
use Category\Catdemo\Models\Addcat;

/**
 * Team Model
 */
class Team extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'rafie_sitepointdemo_teams';
 
     public function getUsersOptions()
    {
        return \Backend\Models\User::lists('login', 'id');
    }

 
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = ['categories' => ['Category\Catdemo\Models\Addcat', 'table' => 'rafie_sitepointdemo_three_cat']];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = ['images' => 'System\Models\File'];


   /*  public function getCategoryIdOptions()
    {
        $model = Addcat::lists('catname', 'id');
       print_r($model);exit;
        return $model;

    }*/
}
