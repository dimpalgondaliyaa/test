<?php namespace Teamcategory\Catdemo\Models;

use Model;

/**
 * Mycat Model
 */
class Mycat extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'teamcategory_catdemo_mycats';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
