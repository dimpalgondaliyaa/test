<?php namespace Teamcategory\Catdemo\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMycatsTable extends Migration
{
    public function up()
    {
        Schema::create('teamcategory_catdemo_mycats', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('teamcategory_catdemo_mycats');
    }
}
